import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentHomePageComponent } from './equipment-home-page.component';

describe('EquipmentHomePageComponent', () => {
  let component: EquipmentHomePageComponent;
  let fixture: ComponentFixture<EquipmentHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EquipmentHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
