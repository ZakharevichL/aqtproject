import {Component, OnInit} from '@angular/core';
import {RestClientService} from '../../../rest-client.service';
import {Category} from '../../../Category';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  public categoryList:Category[] = [];

  constructor(private restClientService: RestClientService) {
  }

  ngOnInit() {
    this.restClientService.getCategoryList().subscribe((category) => {
      this.categoryList = category;
    })
  }

}
