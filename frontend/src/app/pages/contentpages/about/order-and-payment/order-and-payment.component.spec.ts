import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderAndPaymentComponent } from './order-and-payment.component';

describe('OrderAndPaymentComponent', () => {
  let component: OrderAndPaymentComponent;
  let fixture: ComponentFixture<OrderAndPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderAndPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderAndPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
