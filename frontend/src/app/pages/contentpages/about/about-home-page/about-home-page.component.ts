import { Component, OnInit } from '@angular/core';
import { } from '@types/googlemaps';

@Component({
  selector: 'app-about-home-page',
  templateUrl: './about-home-page.component.html',
  styleUrls: ['./about-home-page.component.scss']
})
export class AboutHomePageComponent implements OnInit {
  public map;
  public coordinats;
  public marker;
  public isOpen=false;
  constructor() { }

  ngOnInit() {
  }

  public showMap() {
    if(this.isOpen){
      this.isOpen=false;
      document.getElementById('map').style.display = 'none';
    }
    else {
      this.isOpen=true;
      document.getElementById('map').style.display = 'block';
      this.coordinats = {lat: 53.871056, lng: 27.411877499999946};
      this.map = new google.maps.Map(document.getElementById('map'), {
        center: this.coordinats,
        zoom: 14,
      });
      this.marker = new google.maps.Marker({
        position: this.coordinats,
        map: this.map
      });
    }
  }

}
