import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutHomePageComponent } from './about-home-page.component';

describe('AboutHomePageComponent', () => {
  let component: AboutHomePageComponent;
  let fixture: ComponentFixture<AboutHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
