import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradeMarksComponent } from './trade-marks.component';

describe('TradeMarksComponent', () => {
  let component: TradeMarksComponent;
  let fixture: ComponentFixture<TradeMarksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradeMarksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeMarksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
