import { Component, OnInit } from '@angular/core';
import { } from '@types/googlemaps';
@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit {
  public map;
  public coordinats;
  public marker;
  constructor() { }

  ngOnInit() {
    document.getElementById('map').style.display = 'block';
    this.coordinats = {lat: 53.871056, lng: 27.411877499999946};
    this.map = new google.maps.Map(document.getElementById('map'), {
      center: this.coordinats,
      zoom: 14,
    });
    this.marker = new google.maps.Marker({
      position: this.coordinats,
      map: this.map
    });
  }
}
