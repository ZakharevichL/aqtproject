import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoolsHomePageComponent } from './pools-home-page.component';

describe('PoolsHomePageComponent', () => {
  let component: PoolsHomePageComponent;
  let fixture: ComponentFixture<PoolsHomePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoolsHomePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolsHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
