import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoolDevelopmentComponent } from './pool-development.component';

describe('PoolDevelopmentComponent', () => {
  let component: PoolDevelopmentComponent;
  let fixture: ComponentFixture<PoolDevelopmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoolDevelopmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolDevelopmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
