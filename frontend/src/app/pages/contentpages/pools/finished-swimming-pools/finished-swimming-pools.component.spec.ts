import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishedSwimmingPoolsComponent } from './finished-swimming-pools.component';

describe('FinishedSwimmingPoolsComponent', () => {
  let component: FinishedSwimmingPoolsComponent;
  let fixture: ComponentFixture<FinishedSwimmingPoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishedSwimmingPoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedSwimmingPoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
