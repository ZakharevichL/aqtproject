import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {
  public slideIndex = 1;
  public qwer = 'slideshow__container';
  public id;
  public slides;

  constructor() {
  }

  ngOnInit() {
  }

  public plusSlides(counter,classe,id) {
    this.showSlides(this.slideIndex += counter,classe,id);
  }

  public getClasse(z){
    if(this.id= z) {
      return this.qwer;
    }
    else{
      return;
    }
  }

  public showSlides(counter,classe,id) {
    this.id = id;
    document.getElementById(this.id).style.display = 'block';
    this.slides = document.getElementsByClassName(classe);
    if (counter > this.slides.length) {
      this.slideIndex = 1;
    }
    if (counter < 1) {
      this.slideIndex = this.slides.length;
    }
    for (let i = 0; i < this.slides.length; i++) {
      this.slides[i].style.display = 'none';
    }
    this.slides[this.slideIndex - 1].style.display = 'block';

  }

  public hideSlides(id){
    document.getElementById(id).style.display = 'none';
    this.slideIndex=1;
  }

}
