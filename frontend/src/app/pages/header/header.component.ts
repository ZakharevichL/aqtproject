import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() {
  }


  ngOnInit(): void {
  document.getElementById('menu').className = 'navigate_mobile_disabled';
  }

  public showMenu(): void {
    document.getElementById('menu').className = 'navigate_mobile_enabled';
  }

  public hideMenu(): void {
    document.getElementById('menu').className = 'navigate_mobile_disabled';
  }
}
