export class Category {
  public name: string;
  public id: number;
  public parentId: number;
  public imgSrc: string;
}
