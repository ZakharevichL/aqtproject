import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MDBBootstrapModule} from 'angular-bootstrap-md';

import {AppComponent} from './app.component';
import {HeaderComponent} from './pages/header/header.component';
import {FooterComponent} from './pages/footer/footer.component';
import {HomepageComponent} from './pages/contentpages/homepage/homepage.component';
import {AboutComponent} from './pages/contentpages/about/about.component';
import {PortfolioComponent} from './pages/contentpages/portfolio/portfolio.component';
import {PoolsComponent} from './pages/contentpages/pools/pools.component';
import {EquipmentComponent} from './pages/contentpages/equipment/equipment.component';
import {ServiceComponent} from './pages/contentpages/service/service.component';
import {DeliveryComponent} from './pages/contentpages/about/delivery/delivery.component';
import {OrderAndPaymentComponent} from './pages/contentpages/about/order-and-payment/order-and-payment.component';
import {FinishedSwimmingPoolsComponent} from './pages/contentpages/pools/finished-swimming-pools/finished-swimming-pools.component';
import {PoolDevelopmentComponent} from './pages/contentpages/pools/pool-development/pool-development.component';
import {AllEquipmentComponent} from './pages/contentpages/equipment/all-equipment/all-equipment.component';
import {CareComponent} from './pages/contentpages/equipment/care/care.component';
import {DecorationComponent} from './pages/contentpages/equipment/decoration/decoration.component';
import {FittingsComponent} from './pages/contentpages/equipment/fittings/fittings.component';
import {AboutHomePageComponent} from './pages/contentpages/about/about-home-page/about-home-page.component';
import {EquipmentHomePageComponent} from './pages/contentpages/equipment/equipment-home-page/equipment-home-page.component';
import {PoolsHomePageComponent} from './pages/contentpages/pools/pools-home-page/pools-home-page.component';
import {SertificatesComponent} from './pages/contentpages/about/sertificates/sertificates.component';
import {TradeMarksComponent} from './pages/contentpages/about/trade-marks/trade-marks.component';
import {ReviewsComponent} from './pages/contentpages/about/reviews/reviews.component';
import {RestClientService} from './rest-client.service';
import {HttpClientModule} from '@angular/common/http';
import {CatalogComponent} from './pages/contentpages/catalog/catalog.component';


export const appRoutes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'catalog', component: CatalogComponent},
  {
    path: 'about', children: [
      {path: '', component: AboutHomePageComponent},
      {path: 'delivery', component: DeliveryComponent},
      {path: 'order-and-payment', component: OrderAndPaymentComponent},
      {path: 'sertificates', component: SertificatesComponent},
      {path: 'trade-marks', component: TradeMarksComponent},
      {path: 'reviews', component: ReviewsComponent},

    ], component: AboutComponent
  },
  {path: 'portfolio', component: PortfolioComponent},
  {
    path: 'equipment', children: [
      {path: '', component: EquipmentHomePageComponent},
      {path: 'all-equipment', component: AllEquipmentComponent},
      {path: 'care', component: CareComponent},
      {path: 'decoration', component: DecorationComponent},
      {path: 'fittings', component: FittingsComponent},

    ], component: EquipmentComponent
  },
  {
    path: 'pools', children: [
      {path: '', component: PoolsHomePageComponent},
      {path: 'pool-development', component: PoolDevelopmentComponent},
      {path: 'finished-swimming-pools', component: FinishedSwimmingPoolsComponent},
    ], component: PoolsComponent
  },
  {
    path: 'service', component: ServiceComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomepageComponent,
    AboutComponent,
    PortfolioComponent,
    PoolsComponent,
    EquipmentComponent,
    ServiceComponent,
    DeliveryComponent,
    OrderAndPaymentComponent,
    FinishedSwimmingPoolsComponent,
    PoolDevelopmentComponent,
    AllEquipmentComponent,
    CareComponent,
    DecorationComponent,
    FittingsComponent,
    AboutHomePageComponent,
    EquipmentHomePageComponent,
    PoolsHomePageComponent,
    SertificatesComponent,
    TradeMarksComponent,
    ReviewsComponent,
    CatalogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    MDBBootstrapModule.forRoot(),
    HttpClientModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [RestClientService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
