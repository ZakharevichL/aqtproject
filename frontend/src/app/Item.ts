import {s} from '@angular/core/src/render3';

export class Item {
  public id: number;
  public name: string;
  public discription: string;
  public imgSrc: string;
  public price: string;
  public catalogIds: number[];
  public article: string;
  public characteristicList: object[];
  public characteristicFullList: object[];
}
