import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from './Category';

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  constructor(public http: HttpClient) {
  }

  public getCategoryList(): Observable<Category[]> {
    return (this.http.get('http://localhost:3000/api/categories') as any);
  }
}
