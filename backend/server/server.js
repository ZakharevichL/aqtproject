'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.all('/*', function (req, res, next) {
  if (req.url.startsWith('/Api') || req.url.startsWith('/explorer') || req.url.startsWith('/api')) {
    res.setHeader('Last-Modified', (new Date()).toUTCString());
    return next();
  }
  let isAsset = req.url.match(/\/(.*\.(js|css|map|png|svg|jpg|xlsx|ttf))\??/);
  if (isAsset) {
    return res.sendFile(req.url, {root: path.resolve(__dirname, '..', 'client')});
  }
  res.sendFile('index.html', {root: path.resolve(__dirname, '..', 'client')});

})

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
